package practise

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func Fetchall(){

	start := time.Now()
	ch := make(chan string)

	/*
	内建函数 make 用来为 slice，map 或 chan 类型分配内存和初始化一个对象
	(注意：只能用在这三种类型上)，跟 new 类似，第一个参数也是一个类型而不是
	一个值，跟 new 不同的是，make 返回类型的引用而不是指针，而返回值也依赖
	于具体传入的类型

	new 的作用是初始化一个指向类型的指针(*T)，make 的作用是为 slice，map
	或 chan 初始化并返回引用(T)。
	 */
	for _,url := range os.Args[1:] {
		go fetch(url,ch)
	}
	for range os.Args[1:]{
		fmt.Println(<-ch)
	}
	fmt.Printf("%.2fs elapsed\n",time.Since(start).Seconds())
}

func fetch(url string,ch chan<- string){
	start := time.Now()
	resp,err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err)
		return
	}
	nbytes,err := io.Copy(ioutil.Discard,resp.Body)
	resp.Body.Close()

	if err != nil {
		ch <- fmt.Sprintf("while reading %s : %v",url ,err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs %7d %s",secs,nbytes,url)


}