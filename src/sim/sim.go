package sim

import (
	"fmt"
	"segment"
	"strings"
	"time"
	"util"
)


func createNotice(id int) chan<- string{//用于通信
	c := make(chan string)
	go channelReceive(id,c)
	return c
}

func channelReceive(id int, c chan string)  {
	for {
		fmt.Printf("worker %d item  %s \n",id, <- c)
	}
}

func bufferedChannel(id int) chan<- string {
	c := make(chan string,10)
	go channelReceive(id,c)
	return c
}

func findVec(item string,lines []string)  {
	for _,line := range lines {
		vect := strings.Split(line," ")
		if item == vect[0] {
			fmt.Printf("%s",line)
		}
	}
}
func Acculate(str string)  {
	//vecdict := makevecdict("/Users/guyuefei/Downloads/Tencent_AILab_ChineseEmbedding/Tencent_AILab_ChineseEmbedding.txt")
	lines,_ := util.ReadLine("/Users/guyuefei/Downloads/Tencent_AILab_ChineseEmbedding/Tencent_AILab_ChineseEmbedding.txt")
	items := segment.SegCut(str)//分词
	fmt.Println(items)

	var channels [] chan<- string

	for i := 0;i <len(items);i ++ {
		channels = append(channels,bufferedChannel(i))
	}

	n := 0
	for _,item := range items{
		channels[n] <- item
		go findVec(item,lines)
		time.Sleep(3 * time.Second)
		n ++
	}
	

}

func makevecdict(dictfilepath string)  map[string][]string {
	lines,err := util.ReadLine(dictfilepath)
	//vectdict := make(map[string][]int)
	if err != nil{
		fmt.Println(err)
		return nil
	}
	vectdict := make(map[string][]string)
	for _,line := range lines {
		items := strings.Split(line," ")
		key := items[0]
		values := items[1:]
		vectdict[key] = values
	}

	return vectdict

}
